from django import forms
from .models import *

class UploadForm(forms.Form):
    error_css_class = "error"

    name = forms.CharField(label="File name", max_length=100)
    description = forms.CharField(label="File description", widget=forms.Textarea)
    fileContent = forms.FileField(label="File")

    class Meta:
        model = File

class ShareForm(forms.Form):
    error_css_class = "error"

    username = forms.CharField(label="Username", max_length=100)

class Starform(forms.Form):
    error_css_class = "error"

    isStarred = forms.BooleanField()

    class Meta:
        model = File
