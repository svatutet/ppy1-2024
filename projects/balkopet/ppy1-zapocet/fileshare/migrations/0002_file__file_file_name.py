# Generated by Django 4.1.7 on 2023-04-23 20:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fileshare', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='file',
            name='fileContent',
            field=models.FileField(default=None, upload_to=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='file',
            name='name',
            field=models.CharField(default=None, max_length=100),
            preserve_default=False,
        ),
    ]
