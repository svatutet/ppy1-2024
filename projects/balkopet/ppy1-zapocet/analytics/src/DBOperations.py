import sqlite3

def connect(dbFile: str) -> sqlite3.Connection:
    conn = None
    try:
        conn = sqlite3.connect(dbFile)
    except:
        raise Exception("Error connecting to SQLite database")

    return conn

def getTables(conn: sqlite3.Connection, dbName='main') -> list:
    cursor = conn.cursor()
    cursor.execute(
        'SELECT name FROM sqlite_master WHERE type="table" AND name NOT LIKE "sqlite_%"'
    )
    tables = [str(t[0]) for t in cursor.fetchall()]
    cursor.close()
    if len(tables) == 0:
        print("No tables found in the specified database.")

    return tables


def readColumn(conn: sqlite3.Connection, tableName: str, columnName: str) -> list:
    cursor = conn.cursor()
    cursor.execute(f"SELECT {columnName} FROM {tableName}")
    rows = [i for i in cursor.fetchall()]
    cursor.close()

    return rows

def readColumns(conn: sqlite3.Connection, tableName: str, col1: str, col2: str):
    cursor = conn.cursor()
    cursor.execute(f"SELECT {col1},{col2} FROM {tableName}")
    rows = [i for i in cursor.fetchall()]
    cursor.close()

    return rows

def findValue(rows: list, searchTerm: str) -> list:
    results = []
    for r in rows:
        if searchTerm in str(r):
            results.append(r)

    return results

def countTableRows(conn: sqlite3.Connection, dbName: str, tableName: str) -> int:
    try:
        cursor = conn.cursor()
        cursor.execute(f"select COUNT(*) from {dbName}.{tableName}")
        row = cursor.fetchone()
        num_records = row[0]
        cursor.close()

        return num_records
    
    except Exception as e:
        print("Error counting table rows", e)
        
        return None
    
def colSearch(conn: sqlite3.Connection, tableName: str, columnName: str, searchTerm: str) -> int:
    ret = 0
    rows = readColumn(conn, tableName, columnName)
    results = findValue(rows, searchTerm)
    if len(results) > 0:
        for _ in results:
            ret += 1

    else:
        print(f"No matches found for '{searchTerm}' in '{columnName}' column of '{tableName}' table.")
    
    return ret
